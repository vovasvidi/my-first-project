var fs = require('fs'),
	args = require('yargs').argv,
	pkg = require('./package.json'),
	archiver = require('archiver'),
	assert = require('assert'),
	glob = require('glob'),
	gulp = require('gulp'),
	cleanCSS = require('gulp-clean-css'),
	gutil = require('gulp-util'),
	plugins = require('gulp-load-plugins')(),
	_ = require('lodash-compat'),
	del = require('del'),
	path = require('path'),
	mergeStream = require('merge-stream'),
	runSequence = require('run-sequence'),
	paths = require('./tasks/paths'),
	globs = require('./tasks/globs'),
	gulpCreate = require('./tasks/gulpCreate'),
	gulpRename = require('./tasks/gulpRename'),
	gulpDynamicPages = require('./tasks/gulpDynamicPages'),
	ractiveParse = require('./tasks/gulpRactiveParse'),
	ractiveBundleWidgets = require('./tasks/gulpRactiveBundleWidgets'),
	gulpRenderPage = require('./tasks/gulpRenderPage.js'),
	renderWidgetsPage = require('./tasks/gulpRenderWidgetsPage'),
	buildUseCaseList = require('./tasks/gulp-build-use-case-list'),
	pagesNav = require('./tasks/pagesNav.js'),
	widgetRequireConfig = require('./tasks/widgetRequireConfig.js'),
	renderComponentsPage = require('./tasks/renderComponentsPage.js'),
	concatManifests = require('./node_modules/ractive-foundation/tasks/concatManifests'),
	postcss = require('gulp-postcss'),
	sourcemaps = require('gulp-sourcemaps'),
	autoprefixer = require('autoprefixer'),
	git = require('gulp-git'),
	bump = require('gulp-bump'),
	tag_version = require('gulp-tag-version'),
	shell = require('gulp-shell');

// Server reference, used in multiple gulp tasks.
var liveServer = plugins.liveServer.new('server.js');

// For consistent file builds during watch tasks.
var runTimestamp = Math.round(Date.now() / 1000);

// Used for gulp-header|gulp-footer.
var templateData = {
	version: pkg.version,
	assetsPathPrefix: process.env.LUX_ASSETS_PATH_PREFIX || '/'
};
if (process.env.COMPRESS_LUX === '0') {
	templateData.compressLux = false;
}

var task = function (name) {
	return require(path.join(__dirname, 'tasks', 'gulp-' + name + '.js'));
};

//-----------------------------------------------------------------------------
// Gulp Tasks
//-----------------------------------------------------------------------------


gulp.task('check-node-version', task('check-node-version'));

gulp.task('clean', task('clean'));

gulp.task('lint', ['copy-quality'], task('lint'));

gulp.task('create', function (callback) {
	gulpCreate(callback);
});

gulp.task('rename-widget', function (callback) {
	gulpRename.renameWidget(callback);
});

gulp.task('rename-component', function (callback) {
	gulpRename.renameComponent(callback);
});

// Alias "build" task, which does most of the work.
gulp.task('build', function (callback) {
	runSequence(
		// Disable Node check version to make it running on different node versions.
		// 'check-node-version',
		'clean',
		'dep-check',
		['lint', 'parse-partials', 'concat-components', 'concat-component-templates', 'bundle-widgets'],
		['build-icon-font', 'build-icon-json', 'create-version-file'],
		'sass',
		'concat-css',
		'css-prefix',
		['copy', 'build-documentation'],
		'build-use-case-list',
		['widget-require-config', 'optimize'],
		callback
	);
});

gulp.task('build-mobi', function (callback) {
	runSequence(
		'check-node-version',
		'clean',
		['lint', 'parse-partials', 'concat-components', 'concat-component-templates', 'bundle-widgets'],
		'sass-mobi',
		'css-prefix',
		['copy', 'build-documentation-mobi'],
		callback
	);
});

gulp.task('create-version-file', task('create-version-file'));

gulp.task('optimize', function () {

	if (process.env.COMPRESS_LUX === '0') {
		// Abort optimize step
		gutil.log(gutil.colors.green('COMPRESS_LUX===0, skipping optimize'));
		return true;
	}

	return mergeStream(
		gulp.src('./framework/bundleLux.js')
			.pipe(sourcemaps.init())
			.pipe(plugins.requirejsOptimize(pkg.optimize))
			.pipe(plugins.uglify())
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(paths.compiled)),

		gulp.src(path.join(paths.compiledWidgets, '*.js'))
			.pipe(sourcemaps.init())
			.pipe(plugins.uglify())
			.pipe(sourcemaps.write('.', {
				// to string match for asset-mapfiles
				sourceMappingURLPrefix: '../../compiled/widgets'
			}))
			.pipe(gulp.dest(paths.compiledWidgets))
	);
});

gulp.task('sass', ['sass-concat-whitelisted'], task('sass'));

gulp.task('sass-mobi', ['sass-concat-whitelisted'], task('sass'));

gulp.task('sass-concat-whitelisted', task('sass-concat-whitelisted'));

gulp.task('copy', task('copy'));

gulp.task('concat-components', task('concat-components'));

gulp.task('concat-component-templates', task('concat-component-templates'));

gulp.task('css-rem-to-px', task('rem-to-px'));

gulp.task('concat-css', function () {

	return gulp.src([
		paths.versionFile,
		'./node_modules/pikaday/css/pikaday.css',
		'./node_modules/swiper/dist/css/swiper.min.css',
		'./node_modules/nouislider/distribute/nouislider.min.css',
		path.join(paths.currentInstance, '/sass/lux.tmp.css')
	])
		.pipe(cleanCSS())
		.pipe(plugins.concat('lux.css'))
		.pipe(gulp.dest('./public/'));

});

// quality jshintrc & jscsrc files
gulp.task('copy-quality', function () {
	return gulp.src([
		'node_modules/javascript-quality/.jshintrc',
		'node_modules/javascript-quality/.jscsrc'])
		.pipe(gulp.dest('.'));
});

gulp.task('dep-check', shell.task('node ./scripts/dependencies.js --show-errors'));

gulp.task('parse-partials', function () {
	return gulp.src(paths.partials)
		.pipe(plugins.sort())
		.pipe(ractiveParse('partials.js'))
		.pipe(gulp.dest(paths.compiled));
});

gulp.task('bundle-widgets', function () {
	// use compiledWidgets for source map prefix
	var result = gulp.src([].concat(paths.widgetsJs, paths.widgetsTpl))
		.pipe(ractiveBundleWidgets())
		.pipe(plugins.stripComments());

	return result.pipe(gulp.dest(paths.compiledWidgets));
});

gulp.task('build-documentation', [
		'build-documentation-pages',
		'build-documentation-widgets',
		'build-documentation-components'
	]
);

gulp.task('build-components-manifest', function () {
	return mergeStream(
		gulp.src('./node_modules/ractive-foundation/dist/manifest-rf.json')
			.pipe(gulp.dest(paths.public)),

		gulp.src(paths.base + '/components/**/manifest.json')
			.pipe(concatManifests('manifest-lux.json'))
			.pipe(gulp.dest(paths.public))
	);
});

gulp.task('concat-manifests', ['build-components-manifest'], function () {
	return gulp.src(_(paths.components).map('componentsManifest').flatten().value())
		.pipe(plugins.sort())
		.pipe(concatManifests('manifest-all.json'))
		.pipe(gulp.dest(paths.public));
});

gulp.task('build-documentation-components', ['concat-manifests'], function () {

	var headerTemplate = fs.readFileSync(paths.docs.header, 'UTF-8');
	var footerTemplate = fs.readFileSync(paths.docs.footer, 'UTF-8');

	return gulp.src(paths.public + '/manifest-all.json')
		.pipe(gulp.dest(paths.public))
		.pipe(renderComponentsPage({
			templateUseCases: false,
			type: 'components',
			componentsDir: paths.base + '/components',
			docSrcPath: paths.docs.pages + '/dynamic/component-page.html',
			indexSrcPath: paths.docs.pages + '/dynamic/components.html',
			allCompsSrcPath: paths.docs.pages + '/dynamic/all-components.html',
			assetsPathPrefix: templateData.assetsPathPrefix
		}))
		.pipe(plugins.header(headerTemplate, templateData))
		.pipe(plugins.footer(footerTemplate, templateData))
		.pipe(gulp.dest(paths.public));
});

// Instance aware - build only docs for a instance.
gulp.task('build-documentation-pages', function () {

	var basePath = path.join(paths.currentInstance, paths.docs.pages);

	// Conversion for windows machines, convert backslash to forward slash for pathing.
	// We're actually dealing with URLs, not filesystem paths!
	// Still using path.join because the "paths" variables are also using path.sep.
	// FIXME Resolve clearly WHEN to use URLs vs filesystem paths.
	var normalizedBasePath = basePath.replace(/\\/g, '/');

	// Get an array of all documentation paths we want to work with.
	var allPagesPaths = glob.sync(
		normalizedBasePath + '/**/*.html',
		{ignore: paths.patterns.docs.dynamic}
	);

	// Convert the pages paths into a model for side-nav display.
	var sideNavDataModel = pagesNav.getSideNavDataModel({
		files: glob.sync(normalizedBasePath + '/**/*.html'),
		basePath: normalizedBasePath,
		pagesPath: '/pages'
	});

	// Load once for all files.
	var pageFileTemplate = fs.readFileSync(paths.docs.pageTemplate, 'UTF-8');
	var headerTemplate = fs.readFileSync(paths.docs.header, 'UTF-8');
	var footerTemplate = fs.readFileSync(paths.docs.footer, 'UTF-8');

	return mergeStream(
		gulp.src(allPagesPaths)
			.pipe(gulpRenderPage({
				sideNavDataModel: sideNavDataModel,
				pageFileTemplate: pageFileTemplate,
				basePath: './'
			}))
			.pipe(plugins.rename(function (file) {
				// Remove the basePath for /public destination.
				// This is a filesystem path, so backslashes for windows.
				file.dirname = file.dirname.replace(basePath, '');
			}))
			.pipe(plugins.header(headerTemplate, templateData))
			.pipe(plugins.footer(footerTemplate, templateData))
			.pipe(gulp.dest(path.join(paths.public, 'pages'))),

		gulpDynamicPages({
			sideNavDataModel: sideNavDataModel,
			templateData: templateData
		})
	);

});

// Build mobi site poc page(s).
gulp.task('build-documentation-mobi', ['copy-mobi'], function () {
	return gulp.src(path.join(paths.mobi.rootPath, 'index.html'))
		.pipe(gulp.dest(paths.public));
});

gulp.task('copy-mobi', function () {
	return mergeStream(
		gulp.src(paths.mobi.dataPath + '/**/*').pipe(plugins.copy(paths.public, {prefix: 3})),

		gulp.src('./node_modules/page/page.js').pipe(plugins.copy(paths.vendors, {prefix: 1}))
	);
});

gulp.task('copy-mobi-assets', function () {
	return gulp.src('./instances/singtel/assets/images/logo/icon.png')
		.pipe(plugins.copy(pkg.cordova.options.dir + '/platforms/android/res/drawable', {prefix: 5}));
});

gulp.task('build-use-case-list', function () {
	return gulp.src(paths.public + '/manifest-lux.json')
		.pipe(buildUseCaseList('test-page-urls.json'))
		.pipe(gulp.dest(paths.public));
});

gulp.task('build-documentation-widgets', function () {

	var headerTemplate = fs.readFileSync(paths.docs.header);
	var footerTemplate = fs.readFileSync(paths.docs.footer);

	return gulp.src(paths.widgetsJson)
		.pipe(renderWidgetsPage({
			widgetsDir: paths.widgetsDir,
			publicPath: paths.public,
			docSrcPath: paths.docs.pages + '/dynamic/widgets.html'
		}))
		.pipe(plugins.header(headerTemplate, templateData))
		.pipe(plugins.footer(footerTemplate, templateData))
		.pipe(plugins.concat('widgets.html'))
		.pipe(gulp.dest(paths.public));
});

gulp.task('build-icon-font', function () {

	var fontName = 'lux-icons';

	return gulp.src(path.join(paths.currentInstance + '/assets/fonts/svg/*.svg'))
		.pipe(plugins.sort())
		.pipe(plugins.iconfontCss({
			fontName: fontName,
			path: path.join(paths.currentInstance + '/assets/templates/_icons.template'),
			targetPath: '../../sass/imports/_icons.scss',
			fontPath: 'assets/fonts/'
		}))
		.pipe(plugins.iconfont({
			fontName: fontName,
			// Recommended by author.
			// Append unicode codepoints to icon files in order to always keep the same codepoints on each run.
			prependUnicode: true,
			normalize: true,
			timestamp: runTimestamp
		}))
		.pipe(gulp.dest(paths.currentInstance + '/assets/fonts/'));

});


//Build JSON File for Icon SCSS
gulp.task('build-icon-json', ['build-icon-font'], function () {
	//read icon svg directory
	fs.readdir(path.join(paths.currentInstance, '/assets/fonts/svg'), function (err, files) {
		if (err) {
			return console.log(err);
		}
		var iconNames = [];
		for (var i = 0; i < files.length; i++) {
			var file = files[i];

			if (file.indexOf('.svg') !== -1) {
				iconNames.push({name: file.split('.svg')[0]});
			}
		}
		fs.writeFile('public/icons.json.template', JSON.stringify(iconNames),
			function (err, files) {
				if (err) {
					throw err;
				}
				console.log('It\'s saved!');
			});
	});
});

gulp.task('dep-check', shell.task('node ./scripts/dependencies.js --show-errors'));

gulp.task('server', function (callback) {

	var isStarted = false;

	// Q's promise API is resolve, reject, notify. liveServer uses notify for
	// console.log statements in server.js.
	liveServer.start().then(function () {
	}, function () {
	}, function () {

		if (!isStarted) {

			// Only run this code once.
			// Any time the server logs stuff, this function is executed.
			isStarted = true;

			// Restart if server.js itself is changed.
			gulp.watch(['server.js', './server/controllers/*.js'], liveServer.start);

			callback();

		}

	});

});

gulp.task('open', function () {

	var port = parseInt(fs.readFileSync('port.http'));

	var options = {
		url: 'http://localhost:' + port + '/pages/index.html'
	};

	// A file must be specified as the src when running options.url
	// or gulp will overlook the task. So I'm just using a dummmy file here.
	// First arg is empty string,
	return gulp.src('./gulpfile.js')
		.pipe(plugins.open('', options));

});

gulp.task('unit-test', function () {
	return gulp.src('./test/**.js', {read: false})
		.pipe(plugins.sort())
		.pipe(plugins.mocha({
			reporter: 'mocha-jenkins-reporter',
			reporterOptions: {
				'junit_report_name': 'Unit Tests',
				'junit_report_path': './test/report.xml',
				'junit_report_stack': 1
			}
		}));
});

gulp.task('bdd-test', task('bdd-test'));

gulp.task('copy-framework', function () {
	return task('copy')({
		copyAssets: false,
		copyFramework: true,
		copyVendor: false
	});
});

gulp.task('copy-assets', function () {
	return task('copy')({
		copyAssets: true,
		copyFramework: false,
		copyVendor: false
	});
});

gulp.task('copy-vendor', function () {
	return task('copy')({
		copyAssets: false,
		copyFramework: false,
		copyVendor: true
	});
});

gulp.task('watch-for-dev', function () {
	return task('watch-for-dev')(liveServer);
});

gulp.task('watch', function () {

	// See https://www.npmjs.com/package/gulp-watch
	var watchOptions = {
		read: false,
		usePolling: false
	};

	// Glob files before hand, as watch doesn't accept ignores.
	var watchFiles = glob.sync('{' + globs.srcBuild.join(',') + '}', {ignore: globs.srcBuildIgnore});

	// Watch everything including SASS, and trigger the entire build.
	// TODO Split this up further, as we notice recompile speed issues. Gotta keep it fast.
	// Some issues with adding/removing widgets/components, and triggering separate watch tasks.
	// sass seems to error when not run in parallel.
	plugins.watch(watchFiles, watchOptions, plugins.batch(function (events, cb) {

		var latestFile;

		events.on('data', function (file) {
			latestFile = file;
			gutil.log('watch: source changed file:', file.path);
		});

		events.on('end', function () {
			runSequence('build', 'unit-test', function () {
				liveServer.notify(latestFile);
				cb();
				gutil.log('watch: source finished.');
			});
		});

	}));

	// TODO Need granular watch tasks for: code, docs, scss.
	// Documentation files, but NOT component/widget docs. They're handled above.
	// plugins.watch(globs.docs, watchOptions, plugins.batch(function (events, cb) {

	// 	var latestFile;

	// 	events.on('data', function (file) {
	// 		latestFile = file;
	// 		gutil.log('watch: docs changed:', file.path);
	// 	});

	// 	events.on('end', function () {
	// 		runSequence('build-documentation', function (err) {
	// 			gutil.log('watch: docs changed... finished.');
	// 			liveServer.notify(latestFile);
	// 			cb();
	// 		});
	// 	});

	// }));

});

// For mobi poc, need different watcher.
gulp.task('watch-mobi', function () {

	// See https://www.npmjs.com/package/gulp-watch
	var watchOptions = {
		read: false,
		usePolling: true
	};

	// Watch everything including SASS, and trigger the entire build.
	// TODO Split this up further, as we notice recompile speed issues. Gotta keep it fast.
	// Some issues with adding/removing widgets/components, and triggering separate watch tasks.
	// sass seems to error when not run in parallel.
	plugins.watch(globs.srcBuild, watchOptions, plugins.batch(function (events, cb) {

		var latestFile;

		events.on('data', function (file) {
			latestFile = file;
			gutil.log('watch: source changed file:', file.path);
		});

		events.on('end', function () {
			runSequence('build-mobi', 'unit-test', function (err) {
				gutil.log('watch: source finished.');
				liveServer.notify(latestFile);
				cb();
			});
		});

	}));

});

gulp.task('validate-instance-switch', function (callback) {
	var srcPath = './instances/',
		instances;

	instances = fs.readdirSync(srcPath).filter(function (file) {
		return fs.statSync(path.join(srcPath, file)).isDirectory();
	});

	var envInstance = process.env.LUX_INSTANCE;

	// @deprecated do not use LUX_TELCO use LUX_INSTANCE
	if (_.contains(instances, process.env.LUX_TELCO)) {
		envInstance = process.env.LUX_TELCO;
		gutil.log(gutil.colors.red.bold('Environment variable LUX_TELCO is being deprecated please use LUX_INSTANCE'));
	}

	assert(_.contains(instances, envInstance), 'Please set LUX_INSTANCE environment variable first: ' +
		_.values(instances).join(', '));

	callback();
});

gulp.task('asset-mapfiles', task('asset-mapfiles'));

// TODO: Looks like a gulp-bundle-static-assets?
gulp.task('bundle', ['build'], function (callback) {

	var archive = archiver('zip');
	archive.on('error', function (err) {
		throw err;
	});

	archive.on('end', function (err) {
		callback();
	});

	var bundlePath = __dirname + '/bundle.zip';
	var output = fs.createWriteStream(bundlePath);
	output.on('close', function () {
		gutil.log('Bundle results:', archive.pointer(), 'total bytes');
		gutil.log('Bundle written to:', bundlePath);
	});

	archive.pipe(output);

	archive.bulk([{
		expand: true,
		cwd: __dirname + '/public',
		src: [
			'**/*', '**/.*', '**/*.*',
			'!**/assets/images/dev-only/**/*.*'
		]
	}]);

	archive.finalize();

});

gulp.task('bundle-static-assets', task('bundle-static-assets', function (callback) {
	callback();
}));

gulp.task('bundle-version', function (callback) {
	runSequence('build', 'asset-mapfiles', 'static-components',
		'bundle-version-create-zip', 'bundle-static-assets',  callback);
});

gulp.task('bundle-version-create-zip', function (callback) {
	var archive = archiver('zip');
	archive.on('error', function (err) {
		throw err;
	});

	archive.on('end', function (err) {
		callback();
	});

	var bundlePath = __dirname + '/bundle-version.zip';
	var output = fs.createWriteStream(bundlePath);
	output.on('close', function () {
		gutil.log('Bundle results:', archive.pointer(), 'total bytes');
		gutil.log('Bundle written to:', bundlePath);
	});

	archive.pipe(output);

	archive.bulk([{
		expand: true,
		cwd: __dirname + '/dist',
		src: [
			'**/*', '**/.*', '**/*.*',
			'!**/assets/images/dev-only/**/*.*'
		]
	}]);

	archive.finalize();
});

gulp.task('widget-require-config', function () {

	return widgetRequireConfig({
		basePath: paths.public + '/',
		pattern: path.join(paths.compiledWidgets, '**/*.js'),
		destination: path.join(paths.public, 'core', 'requireConfig.js'),
		findKey: '// BUNDLE FINISH'
	});

});

gulp.task('css-prefix', function () {
	return gulp.src('public/**/*.css')
		.pipe(sourcemaps.init())
		.pipe(postcss([autoprefixer()]))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('public'));
});

gulp.task('static-components', task('static-components'));

//-----------------------------------------------------------------------------
// Gulp Cordova tasks
//-----------------------------------------------------------------------------

gulp.task('cordova-init', function () {
	return mergeStream(
		gulp.src(paths.public)
			.pipe(plugins.cordovaCreate(pkg.cordova.options)),

		gulp.src('package.json')
			.pipe(plugins.cordova(
				pkg.cordova.commands.init, {cwd: pkg.cordova.options.dir}
			))
	);
});

gulp.task('cordova-plugins', function () {
	return gulp.src('package.json')
		.pipe(plugins.cordova(
			pkg.cordova.commands.plugins, {cwd: pkg.cordova.options.dir}
		));
});

gulp.task('cordova-build', function () {
	return gulp.src('package.json')
		.pipe(plugins.cordova(
			pkg.cordova.commands.build, {cwd: pkg.cordova.options.dir}
		));
});

gulp.task('cordova-update-config', function () {
	return gulp.src(paths.mobi.cordovaConfigPath)
		.pipe(gulp.dest(pkg.cordova.options.dir));
});

gulp.task('cordova-copy-public', function () {
	return gulp.src([paths.public + '/*'])
		.pipe(plugins.copy(pkg.cordova.options.dir + '/www', {prefix: 1}));
});

gulp.task('cordova-run', function () {
	return plugins.run('cordova run .cordova')
		.exec();
});

gulp.task('aem-upload-clean', function (callback) {
	return del([
		'./tmp'
	], callback);
});

gulp.task('aem-upload', ['aem-upload-clean', 'build'], function (callback) {
	var aemTools = require('./tasks/gulpAemTools');
	aemTools.upload();
});


//-----------------------------------------------------------------------------
// Gulp core task aliases.
//-----------------------------------------------------------------------------

gulp.task('default', function () {
	runSequence('validate-instance-switch', 'build', 'unit-test', 'server', 'watch');
});

gulp.task('dev', function () {
	runSequence('validate-instance-switch', 'build', 'server', 'watch-for-dev');
});

gulp.task('server-forever-build', function () {
	runSequence('validate-instance-switch', 'build');
});

gulp.task('server-forever-run', function () {
	runSequence('server', 'watch');
});


gulp.task('test', function (cb) {
	runSequence('validate-instance-switch', 'build', 'unit-test', 'bdd-test', cb);
});

gulp.task('ci', function (cb) {
	runSequence('test', 'asset-mapfiles', cb);
});

// For browser dev
gulp.task('mobi', function () {
	var self = this;
	runSequence('validate-instance-switch', 'build-mobi', 'unit-test', 'server', 'watch-mobi', function (err) {
		self.emit('end');
	});
});

// Build and create cordova project
gulp.task('cordova', function () {
	var self = this;
	runSequence(
		'validate-instance-switch',
		['unit-test', 'build-mobi'],
		'cordova-init', 'cordova-plugins', 'cordova-build', 'cordova-update-config', 'cordova-copy-public',
		'copy-mobi-assets',
		function (err) {
			self.emit('end');
		}
	);
});


gulp.task('optimize-png', task('optimize-png'));

//ONLY FOR OPTUSSD NOW @SV 24/02/2016
//TODO Make it parametrized to make it work for singtel, optus and optussd

var optusSDPrefix = paths.luxInstance + '-';
var tagVersion = '';

function inc(importance) {
	return gulp.src(paths.instancePkg)
			// bump the version number in those files
			.pipe(bump({type: importance}))
			// save it back to filesystem
			.pipe(gulp.dest(paths.currentInstance))
			// commit the changed version number
			.pipe(git.commit('Release: Creating Tag release'))
			.pipe(tag_version({prefix: optusSDPrefix}));
}


gulp.task('patch', function () {
	return inc('patch', args.currentBranch);
});
gulp.task('feature', function () {
	return inc('minor', args.currentBranch);
});
gulp.task('project', function () {
	return inc('major', args.currentBranch);
});

/**
 * Sets the version which will be used in tagging or maven deploy.
 * This will read the version from the package json file in the optussd
 */
gulp.task('set-version', function () {
	var packageJsonFile = fs.readFileSync(paths.instancePkg);
	tagVersion = JSON.parse(packageJsonFile.toString()).version;
});

/**
 * Get the version which will be used to deploy the OSG in cross dependant
 * deployment
 * This will read the version from the package json file in the optussd
 */
gulp.task('get-version', function () {
	var packageJsonFile = fs.readFileSync(paths.instancePkg);
	var currentVersion = JSON.parse(packageJsonFile.toString()).version;
	currentVersion = optusSDPrefix + currentVersion + args.snapshot;
	fs.writeFileSync('package-version.txt', currentVersion);
});

/**
 * This function checkouts the tag version which has been set in the set-version
 * task. This can not be called without calling the set-version
 */
gulp.task('checkout', function (cb) {
	return git.checkout(optusSDPrefix + tagVersion,
		function (err) {
			cb(err);
		});
});

/**
 * This function push a tag version to origin
 * This can not be called without calling the set-version
 */
gulp.task('push', function (cb) {
	return git.push('origin', optusSDPrefix + tagVersion,
		function (err) {
			cb(err);
		});
});

gulp.task('push-current-branch', function (cb) {
	return git.push('origin', args.currentBranch,
		function (err) {
			cb(err);
		});
});

gulp.task('checkout-current-branch', function (cb) {
	console.log('args current brach', args.currentBranch);
	return git.checkout(args.currentBranch,
		function (err) {
			cb(err);
		});
});
//Maven Upload for either release or snapshot release
/**
 * This task uploads the build bundle.zip to nexus. This accepts two parameter
 * @ repoId : Example local-snapshot
 * @ repoURL : Example http://localhost:8081/nexus/content/repositories/snapshots
 */
gulp.task('maven-upload', shell.task([
	'mvn deploy:deploy-file ' +
	'-DgroupId=com.singtel.springd ' +
	'-DartifactId=onlinestyleguide ' +
	'-Dversion=<%= getVersion() %>  ' +
	'-Dpackaging=zip -DrepositoryId=<%= getRepoId() %> ' +
	'-Durl=<%= getRepoURL() %>' +
	' -Dfile=<%= getBundleFile() %>' + '<%= getSideFiles() %>'

], {
	templateData: {
		getBundleFile: function () {
			if (args.isVersioned) {
				return 'bundle-version.zip';
			} else {
				return 'bundle.zip';
			}
		},
		getVersion: function () {
			return optusSDPrefix + tagVersion;
		},
		getRepoId: function () {
			return args.repoId;
		},
		getRepoURL: function () {
			return args.repoURL;
		},
		getSideFiles: function () {
			if (args.sideFiles) {
				return ' -Dfiles=' + args.sideFiles + ' -Dtypes=' + args.sideFilesTypes +
					' -Dclassifiers=' + args.sideFileClassifiers;
			}
		}
	}
}));

/**
 * Patch release means defect and hot fixes
 * @ repoId : Example local-snapshot
 * @ repoURL : Example http://localhost:8081/nexus/content/repositories/releases
 * @ currentBranch : The current branch name being built
 * Example : gulp release-patch --repoId=local-snapshot
 *                --repoURL=http://localhost:8081/nexus/content/repositories/releases
 *                --currentBranch=testing
 */
gulp.task('release-patch', function () {
	var bundleVersionTask = '';
	if (args.isVersioned) {
		bundleVersionTask = '-version';
	}
	if (args.skipBundle) {
		runSequence('patch', 'set-version', 'push', 'checkout', 'maven-upload',
			'checkout-current-branch', 'push-current-branch');
	} else {
		runSequence('patch', 'set-version', 'push', 'checkout', 'bundle' + bundleVersionTask, 'maven-upload',
			'checkout-current-branch', 'push-current-branch');
	}

});

/**
 * Feature release means story or functionality release
 * @ repoId : Example local-snapshot
 * @ repoURL : Example http://localhost:8081/nexus/content/repositories/releases
 * @ currentBranch : The current branch name being built
 * Example : gulp release-feature --repoId=local-snapshot
 *                --repoURL=http://localhost:8081/nexus/content/repositories/releases
 */
gulp.task('release-feature', function () {
	var bundleVersionTask = '';
	if (args.isVersioned) {
		bundleVersionTask = '-version';
	}
	if (args.skipBundle) {
		runSequence('feature', 'set-version', 'push', 'checkout', 'maven-upload',
			'checkout-current-branch', 'push-current-branch');
	} else {
		runSequence('feature', 'set-version', 'push', 'checkout', 'bundle' + bundleVersionTask, 'maven-upload',
			'checkout-current-branch', 'push-current-branch');
	}

});

/**
 * Project release means major release for the application
 * @ repoId : Example local-snapshot
 * @ repoURL : Example http://localhost:8081/nexus/content/repositories/releases
 * @ currentBranch : The current branch name being built
 */
gulp.task('release-project', function () {
	var bundleVersionTask = '';
	if (args.isVersioned) {
		bundleVersionTask = '-version';
	}
	if (args.skipBundle) {
		runSequence('release', 'set-version', 'push', 'checkout', 'maven-upload',
			'checkout-current-branch', 'push-current-branch');
	} else {
		runSequence('release', 'set-version', 'push', 'checkout', 'bundle' + bundleVersionTask, 'maven-upload',
			'checkout-current-branch', 'push-current-branch');
	}

});

/**
 * Maven deploy to build the bundle and needs two parameters.
 * Make sure that you pass the repository as snapshot as you are not creating
 * a release.
 * @ repoId : Example local-snapshot
 * @ repoURL : Example http://localhost:8081/nexus/content/repositories/snapshots
 * Example: gulp maven-deploy --repoId=local-snapshot
 *            --repoURL=http://localhost:8081/nexus/content/repositories/snapshots
 */
gulp.task('bundle-maven-deploy', function () {
	runSequence('set-version');
	tagVersion = tagVersion + '-SNAPSHOT';
	console.log('tag version is', tagVersion);
	//This is to improve build time. No need to run bundle-version again
	if (args.skipBundle) {
		runSequence('maven-upload');
	} else {
		var bundleVersionTask = '';
		if (args.isVersioned) {
			bundleVersionTask = '-version';
		}
		runSequence('bundle' + bundleVersionTask, 'maven-upload');
	}

});
